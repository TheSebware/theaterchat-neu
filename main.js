var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var listing = require("express-serve-json-dir");

app.use(express.static("static"));
app.use("/media", express.static("media"));
app.use("/media", listing('media'));

io.on('connection', function(socket){
  console.log('a user connected');
  socket.emit('chat message', {data: "hi"});
  socket.on("command", function(data) {
  	io.emit("command", data);
  	console.log("Command with OpCode " + data.command + " and data \"" + JSON.stringify(data) + "\" was broadcasted.\n\n")
  })
});

http.listen(8081, function(){
  console.log('listening on *:8081');
});