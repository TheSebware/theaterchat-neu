$(function() {
	$('.input-group input, .input-group textarea').each(function() {
		checkInput($(this));
	});
	$('.input-group input, .input-group textarea').focusout(function() {
		checkInput($(this));
	});
});

function checkInput(input) {
	if(input.val() === "") {
		input.removeClass("has-value");
	} else {
		input.addClass("has-value");
	}
}

$(function() {
    setTimeout(function() {
		$('.input-group').addClass('animated');
    }, 1);
});