//var socket = io(location.protocol + "//" + location.hostname + ":" + location.port);
var socket = io();
var connected = false;
var remote_con = false;
var cnt = 0;
function addMsg(data) {
	this.container = $('<section></section>');
	this.container.addClass('message-spacer');
	this.message = $('<div></div>').addClass('message message-other message-signed');
	this.sender = $('<span></span>').addClass('message-sender');
	this.msgBody = $('<span></span>');
	this.msgTime = $('<span></span>').addClass('message-time');
	this.sender.text(data.msgSender);
	this.msgBody.text(data.msgBody);
	this.msgTime.text(data.msgTime);
	this.message.append(this.sender);
	this.message.append(this.msgBody);
	this.message.append(this.msgTime);
	this.container.append(this.message);
	this.container.attr('id', 'msg_' + data.msgID);
	$('#message-area').append(this.container);
	$('html, body').animate({
		scrollTop: this.container.offset().top
	}, 1000);
	console.log(data);
	return this;
}
function toggleBlank(data) {
	if(data.layer == 'chat') {
		$('#layer_blankChat').fadeToggle(data.duration);
	}
	else if(data.layer == 'master') {
		$('#layer_masterBlank').fadeToggle(data.duration);
	}
}
function fadeVideo(cmd) {
	if(data.player == 1) 
		$('#player1').fadeToggle(data.duration);
	else if(data.player == 2) 
		$('#player1').fadeToggle(data.duration);
}
function playVideo(cmd) {
	
}
function parseCommand(cmd) {
	console.log(cmd.command);
	switch(cmd.command) {
		case 1:
		//Show new Message
			console.log("1");
			addMsg(cmd.data.content);
			break;
		case 2:
		//toggle Blank
			toggleBlank(cmd.data.content);
			break;
		case 3:
		//Fade Videoplayer
			fadeVideo(cmd.data.content);
			break;
		case 4:
		//Play Video
			playVideo(cmd.data.content);
			break;
		case 5:
		//Load Video
			loadVideo(cmd.data.content);
			break;
		default:
			break;
	}
}
$(document).ready = function() {
	socket = io();
	socket.on('link', function(msg) {
		alert("link is " + msg.status);
		if(msg.status == up)
			connected = true;
	});
	socket.on('mode', function(msg) {
		if(msg.type == 'cpanel') {
			remote_con = true;
		}
	});
	socket.on('command', function(msg) {
		parseCommand(msg);
	});

	socket.on('chat message', function(msg){
    	console.log(msg);
  	});

	cnt = 0;
	wait = true;
	while(wait) {
		if(!connected) {
			if(cnt > 100) {
				cnTimeout();
				return;
			}
			cnt++;
			$().delay(100, "conTimeOut");
			
		}
		else
			wait = false;
	}
	//socket.io-Verbindung vorhanden

	cnt = 0;
	wait = true;
	while(wait) {
		if(!remote_con) {
			cnt++;
			$().delay(100, "conTimeOut");
		}
		else
			wait = false;
	}
	//CPanel + Client verbunden
}