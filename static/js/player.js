﻿//Usage:
//If you want to control an existing element:
//var pl = new Player(document.getElementById(ID_of_your_audio_element));
//
//If you want to use a new element, use this code:
//var pl = new Player(document.createElement("audio"));
//
function Player(p) {
    this.playerObject = p;

    this.play = function() { this.playerObject.play(); }

    this.load = function (url) {
        this.playerObject.src = url;
        this.playerObject.load();
    }
    
    this.stop = function() { this.pause(); this.setPosition(0); }
    this.pause = function() { this.playerObject.pause(); }

    this.setVolume = function (value) { this.playerObject.volume = value; }
    this.getVolume = function () { return this.playerObject.volume; }

    this.getPosition = function () { return this.playerObject.currentTime; }
    this.setPosition = function (value) { this.playerObject.currentTime = value; }
    this.getLength = function () { return this.playerObject.currentTime; }

    this.hasEnded = function() { return this.playerObject.ended; }

    this.error = function() { return this.playerObject.error; }

    this.isPaused = function() { return this.playerObject.paused; }

    this.isRunning = function () {
        return ((!this.hasEnded()) && (!this.error()) && (!this.isPaused()));
    }
}