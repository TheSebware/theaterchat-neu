function Queue(baseData) {
  if(typeof baseData != "array")
    return null;

  this.messages = baseData;
  this.currentMsg = 0;

  this.getAllMessages = function() {
    return this.messages;
  }

  this.loadMessagesFromString = function(data) {
    this.messages = JSON.parse(data);
  }

  this.getCurrentMessage = function() {
    if(this.messages.length == this.currentMsg) {
      return null;
    }
    return this.messages[currentMsg];
  }

  this.getNextMessage = function() {
    this.currentMsg++;
    return this.getCurrentMessage();
  }

  this.pushMessage = function(message) {
    this.messages.push(message);
  }

  this.popMessage = function() {
    this.messages.pop();
  }

  this.insertMessage = function(message, position) {
    ret = [];
    for(i=0; i<position; i++) {
      ret.push(messages[i]);
    }
    ret.push(message);
    for(i=position; i<messages.length; i++) {
      ret.push(messages[i]);
    }
    messages = ret;
  }

  this.removeMessage = function(position) {
    ret = [];
    for(i=0; i < this.messages.length; i++) {
      if(i != position)
        ret.push(this.messages[i])
    }
    this.messages = ret;
  }

  this.jumpTo = function(position) {
    if(position < 0 || position >= this.messages.length) {
      return null;
    }
    this.currentMsg = position;
  }

  this.getCurrentPosition = function() {
    return this.currentMsg;
  }

  this.getMsgCount = function() {
    return this.messages.length;
  }

}
