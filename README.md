# TheaterChat - Readme

I wrote this for a theater piece of my current (2015-2016) school. So no support at all is given.
It emulates a group conversation in a generic instant messenger app.

A few warnings ahead:

* I'm not good at Node.js coding, I did it because I needed WebSockets
* I'm neither good with Socket.io. If you can do it better, write a patch and hit me up.
* WARNING: Bodges ahead
* DO NOT CONNECT THIS TO THE OPEN INTERNET. Only use it in your local network (or even better: only on your local PC)


## Currently WIP

* Blanking of Screen
* Video playback
* Control Panel
* Importing playlists from a Spreadsheet - will be replaced by a bunch of JSON-Files

## Nice-to-have features

- Keyboard shortcuts
- Integration of Logitech presenters

# Acknowledgements
Credits to whom Credits due:

* [Andr3as07](https://andr3as07.tk) for making the nice chatroom (/static/projector.html and /static/projector_new.html) and the control panel UI (/static/cpanel.html).

* [Eli Grey](http://eligrey.com) for making FileSaver.js.

Thank you.
